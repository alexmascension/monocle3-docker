# monocle3-docker

Monocle 3 is a software developed by Cole Trapnell. The first two versions of Monocle (Monocle and Monocle2) have been widely used for trajectory inference in single-cell analysis, and can be considered as a gold-standard in the downstream analysis of single cell analysis. [Monocle 3](http://cole-trapnell-lab.github.io/monocle-release/monocle3/) was developed recently, and this update includes many interesting things, such as the support of UMAP, trajectories with multiple roots and implementation of loops and points of convergence.

Monocle 3 is supported for R >= 3.5, and due to the huge amount of dependencies, installing it can be a saunting task. In order to make things easier, 
I have packaged Monocle 3 with Rstudio in a docker container to be able to run Monocle 3 in those systems in which installing the program is nearly impossible due
to the dependences.

Also, in the file `Installation script.md` I add the steps I needed to install Monocle 3, including dependencies, R, and R studio.

You can pull the container from docker using 

```
sudo docker pull alexmascension/monocle3
```
And run it using 

```
sudo docker run -it alexmascension/monocle3
```

Remember that if you want to export the results from the analysis into the local disk, you need to add the aprameter -v:

```
sudo docker run -it -v host/directory:docker/directory alexmascension/docker3
```

## Running Rstudio in Docker

Since Rstudio requires a graphical environment, you need to install `x11docker`.

In order to do that, follow these steps:
  - Go to a directory where you will download x11docker install files.
  - `git clone https://github.com/mviereck/x11docker`
  - `bash x11docker/x11docker --install`

Once it has been installed, you can run the container using 

```
x11docker alexmascension/monocle3 Rstudio
```

which will run an Rstudio session with Monocle 3. If you are interested in sharing the host directory with the docker image directory,
check [this link](https://github.com/mviereck/x11docker#shared-folders-and-home-in-container).




