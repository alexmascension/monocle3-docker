# Installation script

This is the script that has been used for the creation of the container.

```bash 

# Install essentials and general dependencies
apt-get update

# Add source to install libpng12-0
echo "deb http://mirrors.kernel.org/ubuntu/ xenial main" >> /etc/apt/sources.list 
apt-get update 

# Continue with dependencies
apt install -y build-essential
apt install -y libssl-dev
apt install -y libxml2-dev
apt install -y python3-pip
apt install -y libudunits2-dev
apt install -y libproj-dev
apt-get install -y software-properties-common
DEBIAN_FRONTEND=noninteractive apt-get -yq install xorg
apt-get install -y libx11-dev
apt-get install -y libglu1-mesa-dev
apt-get install -y libjpeg-dev
apt-get install -y gdebi-core
apt-get install -y aptitude
apt-get install -y gdebi-core
apt-get install -y libpng12-0
apt install -y nano

aptitude install -y libgdal-dev
aptitude install -y -y libproj-dev
aptitude install -y libcurl4-dev
aptitude install -y libfreetype6-dev

# Install R
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
apt update -y
apt install -y r-base r-base-core r-recommended

# Install python dependencies
pip3 install virtualenv
pip3 install umap-learn
pip3 install louvain

# Install rstudio
wget https://download1.rstudio.org/rstudio-xenial-1.1.463-amd64.deb
gdebi rstudio-xenial-1.1.463-amd64.deb
apt-get -f -y install

# Run R and install dependencies
R
install.packages("RCurl")
install.packages("devtools")
install.packages("reticulate")
install.packages("rgdal")
install.packages("rgl")

# Prerrequisites for Monocle 3, and the last line is monocle3_alpha
source("http://bioconductor.org/biocLite.R")
biocLite()
biocLite("monocle")
devtools::install_github("cole-trapnell-lab/DDRTree", ref="simple-ppt-like")
devtools::install_github("cole-trapnell-lab/L1-graph")
devtools::install_github("cole-trapnell-lab/monocle-release", ref="monocle3_alpha")
```
